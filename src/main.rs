mod plugins;

use plugins::ip::Ipv4;
use std::collections::HashMap;
// #[derive(Debug)]

fn main() {
    let ip = Ipv4::new((202, 117, 3, 64), 24);
    ip.print();

    let mut scores = HashMap::new();
    scores.insert("walter".to_string(), 123);
    scores.insert("gus".to_string(), 456);
    println!("{:?}", scores);

    let teams = vec![String::from("Blue"), String::from("Yellow")];
    let initial_scores = vec![10, 50];
    let scores2: HashMap<_, _> = teams.iter().zip(initial_scores.iter()).collect();

    for (key, value) in &scores2 {
        println!("{}: {}", key, *value + 1);
    }

    let text = "hello world wonderful world";
    let mut world_map = HashMap::new();
    for word in text.split_whitespace() {
        let count = world_map.entry(word.to_string()).or_insert(0);
        *count += 1;
    }
    println!("{:?}", world_map);

    let world_times = world_map.get("world");
    match world_times {
        None => println!("none"),
        Some(i) => println!("a: {}", i),
    }
}

pub struct Ipv4 {
    data: (u8, u8, u8, u8),
    subnet: u8,
}

impl Ipv4 {
    pub fn new(ip: (u8, u8, u8, u8), sn: u8) -> Ipv4 {
        Ipv4 {
            data: ip,
            subnet: sn,
        }
    }

    pub fn print(&self) {
        println!(
            "{}.{}.{}.{}/{}",
            self.data.0, self.data.1, self.data.2, self.data.3, self.subnet
        )
    }
}
